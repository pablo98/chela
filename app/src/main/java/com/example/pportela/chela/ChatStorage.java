package com.example.pportela.chela;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.nearby.messages.MessagesOptions;

/**
 * Created by pportela on 13/10/15.
 */
public final class ChatStorage {
    //TODO this has to be removed. Storing static reference to activity objects is bad
    public static DbListener dbListener;

    public static void storeMessage(Context context, ChatMessage message) {
        ChatMessage messages[] = {message};
        ChatStorage.storeMessages(context, messages);
    }

    public static void storeMessages(Context context, ChatMessage[] messages) {
        // Gets the data repository in write mode
        SQLiteOpenHelper mDbHelper = new MessagesDatabaseHelper(context);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Log.d("CHICHA", "Gonna try and insert some messages");

        for (ChatMessage m : messages) {
            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(MessageTableContract.MessageEntry.COLUMN_NAME_TEXT, m.getText());
            values.put(MessageTableContract.MessageEntry.COLUMN_NAME_SENT_AT, m.getSentAt());
            values.put(MessageTableContract.MessageEntry.COLUMN_NAME_RECEIVED_AT, m.getReceivedAt());
            values.put(MessageTableContract.MessageEntry.COLUMN_NAME_SENDER, m.getSender());
            values.put(MessageTableContract.MessageEntry.COLUMN_NAME_RECIPIENT, m.getRecipient());
            // Insert the new row, returning the primary key value of the new row
            long newRowId;
            newRowId = db.insert(
                    MessageTableContract.MessageEntry.TABLE_NAME,
                    null,
                    values);
            Log.d("CHICHA", "Just inserted message with rowid " + Long.toString(newRowId));
        }

        //TODO here we should send an Android broadcast or use a kind of message bus (look for EventBus by Greenrobot)
        if (dbListener != null) {
            dbListener.notify(messages);
        }
    }

    public static Cursor getUserMessages(Context context, String user) {
        SQLiteOpenHelper mDbHelper = new MessagesDatabaseHelper(context);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String where = String.format(
                "%s = ? OR %s = ?",
                MessageTableContract.MessageEntry.COLUMN_NAME_SENDER,
                MessageTableContract.MessageEntry.COLUMN_NAME_RECIPIENT
        );
        qb.setTables(MessageTableContract.MessageEntry.TABLE_NAME);

        Cursor cursor = qb.query(
                db,
                new String[]{
                        MessageTableContract.MessageEntry._ID,
                        MessageTableContract.MessageEntry.COLUMN_NAME_TEXT,
                        MessageTableContract.MessageEntry.COLUMN_NAME_SENT_AT,
                        MessageTableContract.MessageEntry.COLUMN_NAME_SENDER,
                        MessageTableContract.MessageEntry.COLUMN_NAME_RECIPIENT
                },
                where,
                new String[]{user, user},
                null,
                null,
                String.format("%s DESC", MessageTableContract.MessageEntry.COLUMN_NAME_SENT_AT)
        );

        return cursor;
    }

    private ChatStorage(){
    }
}
