package com.example.pportela.chela;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by pportela on 08/10/15.
 */
public class ChatMessage  {
    private String text;
    private String sender;
    private String recipient;
    private long sentAt;
    private long receivedAt;
    private int salt;

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public long getSentAt() {
        return sentAt;
    }

    public long getReceivedAt() {
        return receivedAt;
    }

    public String getText() {
        return text;
    }

    // to be called when message is sent
    public ChatMessage(String sender, String recipient, String text) {
        this.sender = sender;
        this.recipient = recipient;
        this.text = text;
        // no creation date, we will be sending it, so we also put in some salt
        long currentMillis = System.currentTimeMillis();
        sentAt = currentMillis / 1000L;
        salt = (int) currentMillis % 1000;
    }

    // to be called when message is received
    public ChatMessage(String sender, String recipient, String text, String sentAt) {
        this.sender = sender;
        this.recipient = recipient;
        this.text = text;
        this.sentAt = Long.parseLong(sentAt);
        this.receivedAt = System.currentTimeMillis() / 1000L;
    }

    @Override
    public String toString() {
        return text;
    }

    private boolean isValid(String sender, String recipient, String text) {
        if (sender == null) {Log.d("CHICHA","sender null");}
        if (recipient == null) {Log.d("CHICHA","recipient null");}
        if (text.length() == 0) {
            return false;
        }

        if (text.length() > 500) {
            return false;
        }

        return true;
    }

    private String getSignature() throws NoSuchAlgorithmException {
        // estoy seguro de que el gran hermano decripta esto en tres minutos con una 286
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(getSignatureSource().getBytes());
        return messageDigest.digest().toString();
    }

    // buscamos entropía de manera muy básica
    private String getSignatureSource() {
        return String.format(
                "%d%s%s%s",
                (int) sentAt % 1000,
                sender.substring(0, Math.min(sender.length(), 4)),
                recipient.substring(0, Math.min(recipient.length(), 4)),
                text.substring(0, Math.min(text.length(), 4))
        );
    }

    //@todo encriptar todo el mensaje de manera tal que me me caiga un dron por la ventana
    public String getJsonForPost() throws Exception {
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("sender", sender);
        jsonParam.put("recipient",recipient);
        jsonParam.put("text",text);
        jsonParam.put("client_timestamp",sentAt);
        jsonParam.put("signature",getSignature());
        Log.d("CHICHA", "Posting this: " + jsonParam.toString());
        return jsonParam.toString();
    }

    public static ChatMessage[] getInstancesFromJson(String json) throws IOException {
        Log.d("CHICHA", "Vamos a parsear este json: " + json);

        ChatMessage[] chatMessages;
        try
        {
            JSONObject response = new JSONObject(json);
            JSONArray messages = response.getJSONArray("messages");
            int l = messages.length();
            chatMessages = new ChatMessage[l];

            for (int i=0; i<l; i++) {
                JSONObject jsonMessage = messages.getJSONObject(i);
                chatMessages[i] = new ChatMessage(
                        jsonMessage.getString("sender"),
                        jsonMessage.getString("recipient"),
                        jsonMessage.getString("text"),
                        jsonMessage.getString("server_timestamp")
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new IOException("Unable to parse json reply from server: "+e.toString());
        }

        return chatMessages;
    }
}
