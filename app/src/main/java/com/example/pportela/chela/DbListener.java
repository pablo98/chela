package com.example.pportela.chela;

/**
 * Created by pportela on 18/11/15.
 */
public interface DbListener {
    void notify(ChatMessage... messages);
}
