package com.example.pportela.chela;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

public class ContactListActivity extends ListActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {

    // Contact list
    SimpleCursorAdapter mAdapter;
    static final String[] PROJECTION = new String[] {
            ContactsContract.Data._ID,
            ContactsContract.Data.DISPLAY_NAME_PRIMARY
    };
    static final String SELECTION = "((" +
            ContactsContract.Data.DISPLAY_NAME_PRIMARY + " NOTNULL) AND (" +
            ContactsContract.Data.DISPLAY_NAME_PRIMARY + " != '' ))";
    String username;

    // Parameter for the actual chat
    public final static String EXTRA_MESSAGE_SENDER    = "com.example.pportela.chela.SENDER";
    public final static String EXTRA_MESSAGE_RECIPIENT = "com.example.pportela.chela.RECIPIENT";

    // GCM
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this,R.xml.preferences,false);

        initBroadcastReceiver();
        initContactList();
    }

    private void initContactList() {
        // For the cursor adapter, specify which columns go into which views
        String[] fromColumns = {ContactsContract.Data.DISPLAY_NAME_PRIMARY};
        int[] toViews = {android.R.id.text1}; // The TextView in simple_list_item_1

        // Create an empty adapter we will use to display the loaded data.
        // We pass null for the cursor, then update it in onLoadFinished()
        mAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                fromColumns,
                toViews,
                0
        );
        setListAdapter(mAdapter);

        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }

    // Called when a new Loader needs to be created
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return new CursorLoader(this, ContactsContract.Data.CONTENT_URI,
                PROJECTION, SELECTION, null, null);
    }

    // Called when a previously created loader has finished loading
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Swap the new cursor in.  (The framework will take care of closing the
        // old cursor once we return.)
        mAdapter.swapCursor(data);
    }

    // Called when a previously created loader is reset, making the data unavailable
    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mAdapter.swapCursor(null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Cursor cursor = mAdapter.getCursor();
        // Move to the selected contact
        cursor.moveToPosition(position);
        // Get the _ID value
        //long mContactId = cursor.getLong(CONTACT_ID_INDEX);
        // Get the selected LOOKUP KEY
        String recipient = cursor.getString(1);
        // Create the contact's content Uri
        //Uri mContactUri = ContactsContract.Contacts.getLookupUri(mContactId, mContactKey);
        Log.d("CHICHA_DEBUG", "Item cliqueado, posicion " + position);
        // Do something when a list item is clicked
        Intent intent = new Intent(this,MessagingActivity.class);
        intent.putExtra(EXTRA_MESSAGE_SENDER,getUsername());
        intent.putExtra(EXTRA_MESSAGE_RECIPIENT,recipient);
        startActivity(intent);
    }

    private String getUsername() {
        //@todo handle real usernames instead
        if (username == null) {
            TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            username = manager.getDeviceId();
        }
        Log.d("CHICHA","Sender es: "+username);

        return username;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(SettingsFragment.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    // GCM methods
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("CHICHA", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void initBroadcastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(SettingsFragment.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.d("CHICHA","Sent the token like a champ");
                } else {
                    Log.d("CHICHA", "Something went terribly wrong on the token sending process");
                }
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, GCMRegistrationIntentService.class);
            intent.putExtra(EXTRA_MESSAGE_SENDER,getUsername());
            startService(intent);
        }
    }
}
