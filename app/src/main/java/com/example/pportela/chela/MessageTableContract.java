package com.example.pportela.chela;

import android.provider.BaseColumns;

/**
 * Created by pportela on 15/10/15.
 */
public final class MessageTableContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public MessageTableContract() {}

    /* Inner class that defines the table contents */
    public static abstract class MessageEntry implements BaseColumns {
        public static final String TABLE_NAME = "messages";
        public static final String COLUMN_NAME_SENDER = "sender";
        public static final String COLUMN_NAME_RECIPIENT = "recipient";
        public static final String COLUMN_NAME_TEXT = "text";
        public static final String COLUMN_NAME_SENT_AT = "sent_at";
        public static final String COLUMN_NAME_RECEIVED_AT = "received_at";
    }
}
