package com.example.pportela.chela;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by pportela on 12/10/15.
 */
public class MessagesDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "chela";
    private static final int DATABASE_VERSION = 1;
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ", ";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + MessageTableContract.MessageEntry.TABLE_NAME + " (" +
                    MessageTableContract.MessageEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MessageTableContract.MessageEntry.COLUMN_NAME_SENDER + TEXT_TYPE + COMMA_SEP +
                    MessageTableContract.MessageEntry.COLUMN_NAME_RECIPIENT + TEXT_TYPE + COMMA_SEP +
                    MessageTableContract.MessageEntry.COLUMN_NAME_TEXT + TEXT_TYPE + COMMA_SEP +
                    MessageTableContract.MessageEntry.COLUMN_NAME_SENT_AT + INTEGER_TYPE + COMMA_SEP +
                    MessageTableContract.MessageEntry.COLUMN_NAME_RECEIVED_AT + INTEGER_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + MessageTableContract.MessageEntry.TABLE_NAME;

    public MessagesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}