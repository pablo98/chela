package com.example.pportela.chela;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by pportela on 11/10/15.
 */
public class SettingsFragment extends PreferenceFragment {
    public static final String SENT_TOKEN_TO_SERVER = "pref_sent_token_to_server";
    public static final String REGISTRATION_COMPLETE = "pref_registration_complete";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
