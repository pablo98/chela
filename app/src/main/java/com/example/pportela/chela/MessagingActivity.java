package com.example.pportela.chela;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MessagingActivity extends AppCompatActivity implements DbListener {

    //private ArrayAdapter<String> messageListAdapter;
    private SimpleCursorAdapter messageListAdapter;
    private ListView messageList;
    private ChatClient chatClient;
    private String recipient;
    private String sender;

    private final String[] displayColumns = {
        MessageTableContract.MessageEntry.COLUMN_NAME_TEXT,
        MessageTableContract.MessageEntry.COLUMN_NAME_SENT_AT,
        MessageTableContract.MessageEntry.COLUMN_NAME_SENDER,
        MessageTableContract.MessageEntry.COLUMN_NAME_RECIPIENT
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        chatClient = ChatClient.getInstance(sender);
        Log.d("CHICHA","Estamos en el Activity para chatear con "+recipient);

        //getActionBar().setTitle(recipient);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        sender = intent.getStringExtra(ContactListActivity.EXTRA_MESSAGE_SENDER);
        recipient = intent.getStringExtra(ContactListActivity.EXTRA_MESSAGE_RECIPIENT);
        initChatView();
        Log.d("CHICHA", "Nuestro actual destinatario es: " + recipient);
        //TODO here we should register a broadcast receiver or an event receiver of a message bus if you use one (EventBus)
        ChatStorage.dbListener = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //TODO here we should unregister the receiver
        ChatStorage.dbListener = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_messaging, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // Display the fragment as the main content.
            getFragmentManager().beginTransaction()
                    .replace(android.R.id.content, new SettingsFragment())
                    .commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void notify(ChatMessage... messages) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageListAdapter.notifyDataSetChanged(); // refresh the adapter
            }
        });
    }

    private void initChatView() {
        //messageListAdapter = new ArrayAdapter<String>(this,R.layout.view_message);
        Log.i("CHICHA","Sender is "+sender);
        Cursor cursor = ChatStorage.getUserMessages(this,sender);
        int to[] = {R.id.text_sender,R.id.text_text};
        messageListAdapter = new SimpleCursorAdapter(
            this,
            R.layout.view_message,
            cursor,
            displayColumns,
            to,
            0
        );
        messageList = (ListView) findViewById(R.id.list_view_message_list);
        messageList.setAdapter(messageListAdapter);
    }

    public void handleSendMessage(View view) {
        try {
            // gather message
            EditText editText = (EditText) findViewById(R.id.edit_message);
            String text = editText.getText().toString();
            Log.d("CHICHA","Vamos a enviar este mensaje: "+text);

            // create Message object
            ChatMessage message = new ChatMessage(sender,recipient,text);
            Log.d("CHICHA","Ya creamos el objeto ChatMessage");

            // handle UI
            editText.setText("");
            Log.d("CHICHA", "Ya agregamos el objecto a la lista");

            // send it
            chatClient.sendMessage(message);
            Log.d("CHICHA", "Ya enviamos el mensaje");

            ChatStorage.storeMessage(this, message);
        }
        catch (Exception e) {
            //@todo tostada cuando el envío de mensajes vuelca
            Log.e("CHICHA","Rompió bien: "+e.toString());
            e.printStackTrace();
        }
    }
}

