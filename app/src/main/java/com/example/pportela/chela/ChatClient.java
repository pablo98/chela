package com.example.pportela.chela;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pablo Gonzalez Portela on 08/10/15.
 */
public class ChatClient {

    static ChatClient chatClientInstance;
    private String username;

    public static ChatClient getInstance(String username) {
        if (chatClientInstance == null) {
            chatClientInstance = new ChatClient(username);
        }

        return chatClientInstance;
    }

    public static ChatClient getExistingInstance() {
        return chatClientInstance;
    }

    // Bound to it for now and all upcoming eternity
    OkHttpClient client;

    public String getServerHost() {
        return "http://188.226.219.213/";
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public ChatClient(String username) {
        this.username = username;
        client = new OkHttpClient();
        Log.d("CHICHA","ChatClient instanciado...");
    }

    // pops messages and asks ChatStorage to store them
    public void popMessages(final Context context) throws Exception {
        Request request = getRequest("pop_messages","{\"recipient\":\"" + username + "\"}");

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException exception) {
                Log.d("CHICHA", "Algo no ha funcionado: " + exception.toString());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code " + response);

                String textResponse = response.body().string();
                Log.d("CHICHA", "We got these mesages: " + textResponse);

                ChatMessage[] chatMessages = ChatMessage.getInstancesFromJson(textResponse);

                //@todo communicate with the main activity so we store, notify and maybe render them
                // instead of putting workflow logic in here
                ChatStorage.storeMessages(context,chatMessages);
            }
        });
    }

    public void sendMessage(ChatMessage message) throws Exception {
        Request request = getRequest("post_message", message.getJsonForPost());
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException exception) {
                Log.d("CHICHA", "Algo no ha funcionado: " + exception.toString());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                Log.d("CHICHA", response.body().string());
            }
        });
    }

    public void registerUser(String username, String token) throws Exception {
        Request request = getRequest("register_user", "{\"username\":\"" + username + "\",\"token\":\"" + token + "\"}");
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException exception) {
                Log.d("CHICHA","Algo no ha funcionado: "+exception.toString());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                Log.d("CHICHA", response.body().string());
            }
        });
    }

    private Request getRequest(String operation, String payload) {
        Log.d("CHICHA", "Sending message " + payload);
        RequestBody body = RequestBody.create(JSON, payload);
        return new Request.Builder()
                .url(getServerHost() + operation)
                .post(body)
                .build();
    }

    private ChatMessage[] getInstancesFromJson(String json) throws IOException {
        Log.d("CHICHA", "Vamos a parsear este json: " + json);
        ChatMessage[] chatMessages;
        try
        {
            JSONObject response = new JSONObject(json);
            JSONArray messages = response.getJSONArray("messages");
            int l = messages.length();
            chatMessages = new ChatMessage[l];

            for (int i=0; i<l; i++) {
                JSONObject jsonMessage = messages.getJSONObject(i);
                chatMessages[i] = new ChatMessage(
                        jsonMessage.getString("sender"),
                        jsonMessage.getString("recipient"),
                        jsonMessage.getString("text"),
                        jsonMessage.getString("server_timestamp")
                );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new IOException("Unable to parse json reply from server: "+e.toString());
        }

        return chatMessages;
    }
}
